INSERT INTO TOPIC (ID, DESCRIPTION, NAME) VALUES
('Java', 'Learn java basics! From 0 to hero!', 'Java basics'),
('Kotlin', 'Learn kotlin basics! From 0 to hero!', 'Kotlin basics'),
('Python', 'Learn python the hard way!', 'Python Masterclass');

INSERT INTO COURSE (ID, DESCRIPTION, NAME, TOPIC_ID) VALUES
('JavaLambdas', 'Learn funcional programming in Java!', 'Java Lambdas', 'Java'),
('JavaStreams', 'Learn how to work with collectons! Java Streams!', 'Java Streams', 'Java'),
('KotlinBasics', 'Learn Kotlin', 'Kotlin on JVM', 'Kotlin'),
('PythonBasics', 'The easiest language to learn?', 'Python', 'Python');

INSERT INTO LESSON (ID, DESCRIPTION, NAME, COURSE_ID) VALUES
('JavaLambdasOverview', 'What is funcional interface?', 'Java Lambdas overview', 'JavaLambdas'),
('JavaStreamsOverview', 'Why everyone loves it?', 'Java Streams overview', 'JavaStreams'),
('KotlinBasicsJVM', 'Learn Kotlin', 'Kotlin on JVM', 'KotlinBasics'),
('PythonBasicsIFs', 'Lets start with an if statement', 'Python', 'PythonBasics');