package com.course_management_system.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Lesson {

    @Id
    private String id;

    private String name;

    private String description;

    @ManyToOne
    private Course course;
}
