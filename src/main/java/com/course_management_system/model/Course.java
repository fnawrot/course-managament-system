package com.course_management_system.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Course {

    @Id
    private String id;

    @NotNull
    private String name;

    private String description;

    @ManyToOne
    private Topic topic;
}
