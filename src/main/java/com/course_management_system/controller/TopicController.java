package com.course_management_system.controller;

import com.course_management_system.service.TopicService;
import com.course_management_system.service.dto.topic.TopicDTO;
import com.course_management_system.service.dto.topic.UpdateTopicDTO;
import com.course_management_system.service.exception.AlreadyExists;
import com.course_management_system.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/topic")
public class TopicController {

    private final TopicService topicService;

    @Autowired
    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping
    public List<TopicDTO> getAllTopics() {
        return topicService.getAllTopics();
    }

    @GetMapping("/{id}")
    public TopicDTO getTopic(@PathVariable String id) throws NotFound {
        return topicService.getTopic(id);
    }

    @PostMapping
    public TopicDTO addTopic(@RequestBody TopicDTO topic) throws AlreadyExists {
        return topicService.addTopic(topic);
    }

    @DeleteMapping("/{id}")
    public TopicDTO deleteTopic(@PathVariable String id) throws NotFound {
        return topicService.deleteTopic(id);
    }

    @PutMapping("/{id}")
    public TopicDTO updateTopic(@RequestBody UpdateTopicDTO topic, @PathVariable String id) throws NotFound {
        return topicService.updateTopic(topic, id);
    }
}
