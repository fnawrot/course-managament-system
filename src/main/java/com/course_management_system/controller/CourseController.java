package com.course_management_system.controller;

import com.course_management_system.model.Course;
import com.course_management_system.service.CourseService;
import com.course_management_system.service.dto.course.CourseDTO;
import com.course_management_system.service.dto.course.UpdateCourseDTO;
import com.course_management_system.service.exception.AlreadyExists;
import com.course_management_system.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/course")
public class CourseController {

    private CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public List<CourseDTO> getAllCoursesByTopicId(@RequestParam("topicId") String topicId) {
        return courseService.getAllCoursesByTopicId(topicId);
    }

    @GetMapping("/{courseId}")
    public CourseDTO getCourse(@PathVariable String courseId) throws NotFound {
        return courseService.getCourse(courseId);
    }

    @PostMapping
    public CourseDTO addCourse(@RequestBody Course course) throws AlreadyExists {
        return courseService.addCourse(course);
    }

    @PutMapping("/{id}")
    public CourseDTO updateCourses(@RequestBody UpdateCourseDTO course, @PathVariable String id) throws NotFound {
        return courseService.updateCourse(course, id);
    }

    @DeleteMapping("/{id}")
    public CourseDTO deleteCourse(@PathVariable String id) throws NotFound {
        return courseService.deleteCourse(id);
    }
}
