package com.course_management_system.controller;

import com.course_management_system.model.Lesson;
import com.course_management_system.service.LessonService;
import com.course_management_system.service.dto.lesson.LessonDTO;
import com.course_management_system.service.dto.lesson.UpdateLessonDTO;
import com.course_management_system.service.exception.AlreadyExists;
import com.course_management_system.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("api/v1/lesson")
public class LessonController {

    private LessonService lessonService;

    @Autowired
    public LessonController(LessonService lessonService) {
        this.lessonService = lessonService;
    }


    @GetMapping
    public List<LessonDTO> getAllLessonsByCourseId(@PathParam("courseId") String courseId) {
        return lessonService.getAllLessonsByCourseId(courseId);
    }

    @GetMapping("/{lessonId}")
    public LessonDTO getLesson(@PathVariable String lessonId) throws NotFound {
        return lessonService.getLesson(lessonId);
    }

    @PostMapping
    public LessonDTO addLesson(@RequestBody Lesson lesson) throws AlreadyExists {
        return lessonService.addLesson(lesson);
    }

    @PutMapping("/{id}")
    public LessonDTO updateLesson(@RequestBody UpdateLessonDTO lesson, @PathVariable String id) throws NotFound {
        return lessonService.updateLesson(lesson, id);
    }

    @DeleteMapping("/{id}")
    public LessonDTO deleteLesson(@PathVariable String id) throws NotFound {
        return lessonService.deleteLesson(id);
    }
}
