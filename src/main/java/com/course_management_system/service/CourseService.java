package com.course_management_system.service;

import com.course_management_system.model.Course;
import com.course_management_system.service.dto.course.CourseDTO;
import com.course_management_system.service.dto.course.UpdateCourseDTO;
import com.course_management_system.service.exception.AlreadyExists;
import com.course_management_system.service.exception.NotFound;
import com.course_management_system.service.mapper.CourseMapper;
import com.course_management_system.service.respository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseService {

    private final CourseRepository courseRepository;
    private final TopicService topicService;
    private final CourseMapper mapper;

    @Autowired
    public CourseService(CourseRepository courseRepository, TopicService topicService, CourseMapper mapper) {
        this.courseRepository = courseRepository;
        this.topicService = topicService;
        this.mapper = mapper;
    }

    public List<CourseDTO> getAllCoursesByTopicId(String topicId) {
        return courseRepository.findByTopicId(topicId).stream()
                .map(mapper::modelToDto)
                .collect(Collectors.toList());
    }

    public CourseDTO getCourse(String courseId) throws NotFound {
        return courseRepository.findById(courseId)
                .map(mapper::modelToDto)
                .orElseThrow(NotFound::new);
    }


    public CourseDTO addCourse(Course course) throws AlreadyExists {
        Course courseOrNull = courseRepository.findById(course.getId()).orElse(null);
        if (courseOrNull != null) {
            throw new AlreadyExists();
        }
        courseRepository.save(course);
        return mapper.modelToDto(course);
    }

    public CourseDTO updateCourse(UpdateCourseDTO course, String id) throws NotFound {
        Course toUpdate = courseRepository.findById(id).orElseThrow(NotFound::new);
        toUpdate.setDescription(course.getDescription());
        toUpdate.setName(course.getName());
        toUpdate.setTopic(course.getTopic());
        courseRepository.save(toUpdate);
        return mapper.modelToDto(toUpdate);
    }

    public CourseDTO deleteCourse(String id) throws NotFound {
        Course course = courseRepository.findById(id).orElseThrow(NotFound::new);
        return mapper.modelToDto(course);
    }
}
