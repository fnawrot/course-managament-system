package com.course_management_system.service;

import com.course_management_system.model.Topic;
import com.course_management_system.service.dto.topic.TopicDTO;
import com.course_management_system.service.dto.topic.UpdateTopicDTO;
import com.course_management_system.service.exception.AlreadyExists;
import com.course_management_system.service.exception.NotFound;
import com.course_management_system.service.mapper.TopicMapper;
import com.course_management_system.service.respository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TopicService {

    private final TopicRepository topicRepository;
    private final TopicMapper mapper;

    @Autowired
    public TopicService(TopicRepository topicRepository, TopicMapper mapper) {
        this.topicRepository = topicRepository;
        this.mapper = mapper;
    }

    public List<TopicDTO> getAllTopics() {
        return topicRepository.findAll().stream()
                .map(mapper::modelToDto)
                .collect(Collectors.toList());
    }

    public TopicDTO getTopic(String id) throws NotFound {
        return topicRepository.findById(id)
                .map(mapper::modelToDto)
                .orElseThrow(NotFound::new);
    }


    public TopicDTO addTopic(TopicDTO topic) throws AlreadyExists {
        Topic topic1 = topicRepository.findById(topic.getId())
                .orElse(null);
        if (topic1 != null) {
            throw new AlreadyExists();
        }
        topicRepository.save(mapper.dtoToModel(topic));
        return topic;
    }

    public TopicDTO updateTopic(UpdateTopicDTO topic, String id) throws NotFound {
        Topic topicToUpdate = topicRepository.findById(id).orElseThrow(NotFound::new);
        topicToUpdate.setName(topic.getName());
        topicToUpdate.setDescription(topic.getDescription());
        topicRepository.save(topicToUpdate);
        return mapper.modelToDto(topicToUpdate);
    }

    public TopicDTO deleteTopic(String id) throws NotFound {
        Topic topicToDelete = topicRepository.findById(id).orElseThrow(NotFound::new);
        topicRepository.delete(topicToDelete);
        return mapper.modelToDto(topicToDelete);
    }
}
