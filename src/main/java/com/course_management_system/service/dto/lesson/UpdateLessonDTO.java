package com.course_management_system.service.dto.lesson;

import com.course_management_system.model.Course;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateLessonDTO {
    private String name;
    private String description;
    private Course course;
}
