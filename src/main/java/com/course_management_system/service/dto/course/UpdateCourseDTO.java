package com.course_management_system.service.dto.course;

import com.course_management_system.model.Topic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCourseDTO {
    private String name;
    private String description;
    private Topic topic;
}
