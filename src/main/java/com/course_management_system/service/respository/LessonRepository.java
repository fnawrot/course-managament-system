package com.course_management_system.service.respository;

import com.course_management_system.model.Lesson;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LessonRepository extends CrudRepository<Lesson, String> {

   List<Lesson> findByCourseId(String courseId);
}
