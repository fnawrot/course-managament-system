package com.course_management_system.service.respository;

import com.course_management_system.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, String> {

   List<Course> findByTopicId(String topicId);
}
