package com.course_management_system.service.mapper;

import com.course_management_system.model.Course;
import com.course_management_system.model.Topic;
import com.course_management_system.service.dto.course.CourseDTO;
import org.springframework.stereotype.Component;

@Component
public class CourseMapper {

    public CourseDTO modelToDto(Course course) {
        return new CourseDTO(course.getId(), course.getName(), course.getDescription());
    }


    public Course dtoToModel(CourseDTO course, Topic topic) {
        return new Course(course.getId(), course.getName(), course.getDescription(), topic);
    }
}
