package com.course_management_system.service.mapper;

import com.course_management_system.model.Topic;
import com.course_management_system.service.dto.topic.TopicDTO;
import org.springframework.stereotype.Component;

@Component
public class TopicMapper {

    public TopicDTO modelToDto(Topic topic) {
        return new TopicDTO(topic.getId(), topic.getName(), topic.getDescription());
    }

    public Topic dtoToModel(TopicDTO topic) {
        return new Topic(topic.getId(), topic.getName(), topic.getDescription());
    }
}
