package com.course_management_system.service.mapper;

import com.course_management_system.model.Course;
import com.course_management_system.model.Lesson;
import com.course_management_system.service.dto.lesson.LessonDTO;
import org.springframework.stereotype.Component;

@Component
public class LessonMapper {

    public LessonDTO modelToDto(Lesson lesson) {
        return new LessonDTO(lesson.getId(), lesson.getName(), lesson.getDescription());
    }

    public Lesson dtoToModel(LessonDTO lessonDTO, Course course) {
        return new Lesson(lessonDTO.getId(), lessonDTO.getName(), lessonDTO.getDescription(), course);
    }


}
