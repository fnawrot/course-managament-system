package com.course_management_system.service;

import com.course_management_system.model.Lesson;
import com.course_management_system.service.dto.lesson.LessonDTO;
import com.course_management_system.service.dto.lesson.UpdateLessonDTO;
import com.course_management_system.service.exception.AlreadyExists;
import com.course_management_system.service.exception.NotFound;
import com.course_management_system.service.mapper.LessonMapper;
import com.course_management_system.service.respository.LessonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LessonService {

    private final LessonRepository lessonRepository;

    private final CourseService courseService;

    private final LessonMapper mapper;

    @Autowired
    public LessonService(LessonRepository lessonRepository, CourseService courseService, LessonMapper mapper) {
        this.lessonRepository = lessonRepository;
        this.courseService = courseService;
        this.mapper = mapper;
    }

    public List<LessonDTO> getAllLessonsByCourseId(String courseId) {
         return lessonRepository.findByCourseId(courseId)
                .stream()
                .map(mapper::modelToDto)
                .collect(Collectors.toList());
    }

    public LessonDTO getLesson(String lessonId) throws NotFound {
        return lessonRepository.findById(lessonId)
                .map(mapper::modelToDto)
                .orElseThrow(NotFound::new);
    }


    public LessonDTO addLesson(Lesson lesson) throws AlreadyExists {
        Lesson lessonToAdd = lessonRepository.findById(lesson.getId()).orElse(null);
        if (lessonToAdd != null) {
            throw new AlreadyExists();
        }
        lessonRepository.save(lesson);
        return mapper.modelToDto(lesson);
    }

    public LessonDTO updateLesson(UpdateLessonDTO lesson, String id) throws NotFound {
        Lesson lessonToUpdate = lessonRepository.findById(id).orElseThrow(NotFound::new);
        lessonToUpdate.setName(lesson.getName());
        lessonToUpdate.setDescription(lesson.getDescription());
        lessonToUpdate.setCourse(lesson.getCourse());
        lessonRepository.save(lessonToUpdate);
        return mapper.modelToDto(lessonToUpdate);
    }

    public LessonDTO deleteLesson(String lessonId) throws NotFound {
        Lesson lesson = lessonRepository.findById(lessonId).orElseThrow(NotFound::new);
        lessonRepository.delete(lesson);
        return mapper.modelToDto(lesson);
    }
}
