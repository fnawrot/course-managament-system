# Course Management System

REST API for managing courses, including lessons and topics (3 entities, connected by ManyToOne unidirectional relationship).

## Built with

- Maven
- Spring
- JPA/Hibernate
- Lombok
- embedded H2 database

## Installation

- Download project from git
- Open project and start SpringDataApplication
- open http://localhost:8080/swagger-ui.html# to visualize and interact with the API's resource